# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 4.1.3

- patch: Update the Readme with a new Atlassian Community link.

## 4.1.2

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 4.1.1

- patch: Fixed the handling of the downstream pipelines that halted.

## 4.1.0

- minor: Added a warning message when new version of the pipe is available

## 4.0.5

- patch: Fixed the handling of the downstream pipelines that have a manual step in their configuration.

## 4.0.4

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 4.0.3

- patch: Updated readme BITBUCKET_USERNAME should be an account name, not the email.

## 4.0.2

- patch: Fixed the bug with parsing yaml when special characters are present.

## 4.0.1

- patch: Added code style checks

## 4.0.0

- major: Rollback to using ACCOUNT instead of WORKSPACE

## 3.1.1

- patch: Internal maintenance: update pipes toolkit version.

## 3.1.0

- minor: Fixed a small bug when DEBUG was True by default

## 3.0.2

- patch: Fixed the incorrect error message when workspace, repository or branch doesn't exist

## 3.0.1

- patch: Documentation improvements

## 3.0.0

- major: PIPELINE_TYPE and PIPELINE_PATTERN parameters were replaced by a single CUSTOM_PIPELINE_NAME parameter

## 2.0.1

- patch: Documentation updates

## 2.0.0

- major: Variables names for REPO, ACCOUNT and APP_PASSWORD were changed
- minor: Add support for passing variables to a custom pipeline
- minor: Added support for triggering pipelines in repos that belong to a team

## 1.1.0

- minor: Added support for passing variables to a custom pipeline

## 1.0.0

- major: Parameters names were changes to be more intuitive. Added support to trigger different pipelines definitions

## 0.3.0

- minor: Don't use pipe.yml to specify parameters schema

## 0.2.1

- patch: Patch version bump

## 0.2.0

- minor: Rename the pipe

## 0.1.0

- minor: Initial release

